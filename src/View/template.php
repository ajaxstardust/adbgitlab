<?php
namespace Adb\View;

use Adb\Controller\Navview  as Navview;
use Adb\Model\Auxx          as Auxx;
use Adb\Model\Navfactor     as Navfactor;
use Adb\Model\Dirhandler    as Dirhandler;


$Dirhandler =               new Dirhandler(TEST_DIRECTORY);
$Auxx =                     new Auxx(ADBLOCTN);

// $Dirhandler->createCharacterArray("","","");
// $htmlCharacterArray =       $Navfactor->htmlCharacterArray;
$htmlCharacterArray = $Dirhandler->createCharacterArray($Dirhandler->readDirectory());
$Navview =                  new Navview($htmlCharacterArray);
$Navfactor =                new Navfactor($htmlCharacterArray);


require NS_ROOT . '/src/View/html-doctype-head.page.php';
require NS_ROOT . '/src/View/html-body-top.page.php';
require NS_ROOT . '/src/View/html-maincontrols.page.php';
require NS_ROOT . '/src/View/html-nav.page.php';
require NS_ROOT . '/src/View/html-main.page.php';
// var_dump(get_defined_vars());
require NS_ROOT . '/src/View/html-footer.page.php'; 

?>