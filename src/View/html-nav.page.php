<?php

namespace Adb\View;

use Adb\Controller\Navcontroller 	as Navcontroller;
use Adb\Controller\Navview 			as Navview;
use Adb\Model\Adbsoc 				as Adbsoc;
use Adb\Model\Auxx 					as Auxx;
use Adb\Model\Backlinks 			as Backlinks;
use Adb\Model\Cwthumbs 				as Cwthumbs;
use Adb\Model\Dirhandler 			as Dirhandler;
use Adb\Model\Htmldochead 			as Htmldochead;
use Adb\Model\Iframe 				as Iframe;
use Adb\Model\Navfactor 			as Navfactor;
use Adb\Model\Urlprocessor 			as Urlprocessor;

/**
* @param MVC: Controller
*/
$Navcontroller 	= new Navcontroller(TEST_DIRECTORY);
$Navview 		= new Navview(TEST_DIRECTORY);

/**
* @param MVC: Model
*/

$Adbsoc 		= new Adbsoc($pathOps);
$Auxx			= new Auxx(NS_ROOT);
$Backlinks 		= new Backlinks();
$thumbs 		= new Cwthumbs;
$Dirhandler 	= new Dirhandler(TEST_DIRECTORY);
$Htmldochead 	= new Htmldochead($pathOps);
$Iframe 		= new Iframe;
$Navfactor 		= new Navfactor(TEST_DIRECTORY);
$Urlprocessor 	= new Urlprocessor($pathOps);

	$arrayObjectAnchors = $Auxx->objectParser();
	$Dir_Contents = $Dirhandler->readDirectory();

	
	
	$buildNavController = $Navcontroller->displayNavigation();

	$currentUrlPath = $Urlprocessor->chopUrl();
	$defaultIframe = $Iframe->defaultIframe;

	$getHtmlPrint = $Navfactor->getHtmlPrint();
	$getImaegs = $thumbs->getImages();
	$htmlCharacterValues = $Navfactor->htmlCharacterArray;
	$pathInfoBasename = $Htmldochead->pathInfoBasename;
	$groupToggler = $Navfactor->groupToggler($Dir_Contents);

	echo $Navfactor->initNav()[0];
    
	// $groupToggler = $Navfactor->groupToggler(CONTENTS);
	
    foreach ($htmlCharacterValues as $key => $value) {
		
		$navItemOuter = $value;	
			
		$many = is_array($value) ? count($value) : 0;
			echo '<li onclick="showHide(\'ul_' . $key . '\')" id="li_' . $key . '_control" class="toggler"><span style="font-weight:bold;">' . $key . '</span> [ view ' . $many . ' ] <ul class="inner">';
			if(is_array($value)){
				
				foreach($value as $target_html){
					echo $target_html;
				}
				echo '</ul>';
				if(count($htmlCharacterArray[$key]) > 0) {
					foreach($htmlCharacterArray[$key] as $listHtml){
						// echo $listHtml;
					}			
			    }			
			else{
				// echo '</li>'
				// echo $Navfactor->groupToggler($Dir_Contents);
			}
		}
		
	}
    
    
?>
</ul>
</nav>
<div id="var_dump">
	<?php
	// echo '<br>var_dump(get_defined_vars()) '. var_dump(get_defined_vars());
	?>
</div>

 